package sbu.cs.multithread.pi;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
public class BBP {
    private static BigDecimal newton = new BigDecimal(0,new MathContext(1001, RoundingMode.HALF_DOWN));
    synchronized public static void adding(BigDecimal bigDecimal){
        newton = newton.add(bigDecimal);
    }
    public static BigDecimal getNewton(){
        return newton;
    }
}
