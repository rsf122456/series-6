package sbu.cs.multithread.pi;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class PICalculator {

    /**
     * calculate pi and represent it as string with given floating point number (numbers after .)
     * check test cases for more info
     * check pi with 1000 digits after floating point at https://mathshistory.st-andrews.ac.uk/HistTopics/1000_places/
     *
     * @param floatingPoint number of digits after floating point
     * @return pi in string format
     */
    private ExecutorService executor = Executors.newFixedThreadPool(16);

    public String calculate(int floatingPoint) {
        for (int i = 0; i < floatingPoint +  5; i++){
            executor.submit(new Calculating(i));
        }
        executor.shutdown();
        try {
            executor.awaitTermination(10000, TimeUnit.MILLISECONDS);
            executor.shutdownNow();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return BBP.getNewton().toString().substring(0,floatingPoint + 2);
    }
}
