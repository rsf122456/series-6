package sbu.cs.multithread.pi;

import java.math.*;

public class Calculating implements Runnable{
    private int k;
    final BigDecimal one = new BigDecimal(1);
    final BigDecimal num2 = new BigDecimal(2);
    final BigDecimal num4 = new BigDecimal(4);
    final BigDecimal num16 = new BigDecimal(16);
    public Calculating(int k){
        this.k = k;
    }
    private BigDecimal call(int first, int second){
        BigDecimal num = switch (second) {
            case 4 -> new BigDecimal((8 * first) + 1);
            case 3 -> new BigDecimal((8 * first) + 5);
            case 2 -> new BigDecimal((8 * first) + 4);
            case 1 -> new BigDecimal((8 * first) + 6);
            default -> null;
        };
        return num;
    }
    @Override
    public void run() {
        BigDecimal first = num4.divide(call(k,4), new MathContext(1001, RoundingMode.HALF_DOWN));
        BigDecimal second = num2.divide(call(k,2), new MathContext(1001, RoundingMode.HALF_DOWN));
        BigDecimal third = one.divide(call(k,3), new MathContext(1001, RoundingMode.HALF_DOWN));
        BigDecimal forth = one.divide(call(k,1), new MathContext(1001, RoundingMode.HALF_DOWN));
        BigDecimal subtract = first.subtract(second).subtract(third);
        BigDecimal subtract1 = subtract.subtract(forth);
        BigDecimal hasel1 = subtract1.divide(num16.pow(k),new MathContext(1001, RoundingMode.HALF_DOWN));
        BBP.adding(hasel1);
    }

}
