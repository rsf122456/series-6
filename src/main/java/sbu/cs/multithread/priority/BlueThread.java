package sbu.cs.multithread.priority;
import java.util.concurrent.*;
public class BlueThread extends ColorThread {
    private static final String MESSAGE = "hi back blacks, hi whites";
    private CountDownLatch blueThenWhite;
    public BlueThread(CountDownLatch countDownLatch){
        blueThenWhite = countDownLatch;
    }
    void printMessage() {
        super.printMessage(new Message(this.getClass().getName(), getMessage()));
    }
    @Override
    String getMessage() {
        return MESSAGE;
    }
    @Override
    public void run() {
        printMessage();
        blueThenWhite.countDown();
    }
}
