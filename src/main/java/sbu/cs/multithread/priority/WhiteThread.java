package sbu.cs.multithread.priority;
import java.util.concurrent.*;
public class WhiteThread extends ColorThread {
    private static final String MESSAGE = "hi back blacks, hi back blues";
    private CountDownLatch nowWhite;
    public WhiteThread(CountDownLatch nowWhite){
        this.nowWhite = nowWhite;
    }
    void printMessage() {
        super.printMessage(new Message(this.getClass().getName(), getMessage()));
    }
    @Override
    String getMessage() {
        return MESSAGE;
    }
    @Override
    public void run() {
        printMessage();
        nowWhite.countDown();
    }
}
