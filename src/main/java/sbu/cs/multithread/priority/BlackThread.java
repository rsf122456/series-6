package sbu.cs.multithread.priority;
import java.util.concurrent.*;
public class BlackThread extends ColorThread {
    private static final String MESSAGE = "hi blues, hi whites";
    private CountDownLatch firstBlackThenBlue;
    public BlackThread(CountDownLatch countDownLatch){
        firstBlackThenBlue = countDownLatch;
    }
    void printMessage() {
        super.printMessage(new Message(this.getClass().getName(), getMessage()));
    }
    @Override
    String getMessage() {
        return MESSAGE;
    }
    @Override
    public void run() {
        printMessage();
        firstBlackThenBlue.countDown();
    }
}
