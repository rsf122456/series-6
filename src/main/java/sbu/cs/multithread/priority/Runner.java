package sbu.cs.multithread.priority;
import java.util.*;
import java.util.concurrent.*;
public class Runner {
    public static List<Message> messages = new ArrayList<>();
    /**
     * add your codes to this function. this function is the caller function which will be called first.
     * changing other codes in this function is allowed.
     *
     * @param blackCount    number of black threads
     * @param blueCount     number of blue threads
     * @param whiteCount    number of white threads
     */
    public void run(int blackCount, int blueCount, int whiteCount) throws InterruptedException {
        List<ColorThread> colorThreads = new ArrayList<>();
        final CountDownLatch MyBlackThreads = new CountDownLatch(blackCount);
        final CountDownLatch MyBlueThreads = new CountDownLatch(blueCount);
        final CountDownLatch MyWhiteThreads = new CountDownLatch(whiteCount);
        for (int i = 0; i < blackCount; i++) {
            BlackThread blackThread = new BlackThread(MyBlackThreads);
            colorThreads.add(blackThread);
            blackThread.start();
        }
        MyBlackThreads.await();
        for (int i = 0; i < blueCount; i++) {
            BlueThread blueThread = new BlueThread(MyBlueThreads);
            colorThreads.add(blueThread);
            blueThread.start();
        }
        MyBlueThreads.await();
        for (int i = 0; i < whiteCount; i++) {
            WhiteThread whiteThread = new WhiteThread(MyWhiteThreads);
            colorThreads.add(whiteThread);
            whiteThread.start();
        }
        MyWhiteThreads.await();
    }
    synchronized public static void addToList(Message message) {
        messages.add(message);
    }
    public List<Message> getMessages() {
        return messages;
    }
    /**
     *
     * @param args
     */
    public static void main(String[] args) {

    }
}
